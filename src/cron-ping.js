const cron1 = require('node-cron');
const cron2 = require('node-cron');

// Schedule tasks to be run on the server.
cron1.schedule('*/10 * * * * *', function() {
    console.log('running a task every 10 seconds');
});

// Schedule tasks to be run on the server.
cron2.schedule('*/2 * * * * *', function() {
    console.log('running a task every 2 seconds');
});